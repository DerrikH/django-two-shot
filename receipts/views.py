from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_objects": receipts
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def show_expense(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_object": expense,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def show_accounts(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_object": account
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {
        "form": form
    }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "accounts/create.html", context)
