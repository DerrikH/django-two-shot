from django.urls import path
from receipts.views import receipt_list, create_receipt, show_expense
from receipts.views import show_accounts, create_expense, create_account

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense, name="category_list"),
    path("accounts/", show_accounts, name="account_list"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
